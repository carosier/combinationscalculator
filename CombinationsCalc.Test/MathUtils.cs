using System;
using MathNet.Numerics;

namespace CombinationsCalc.Test
{
    public static class MathUtils
    {
        public static int NumberOfCombination(int totalElements, int elementsPerCombination) =>
            Convert.ToInt32(SpecialFunctions.Factorial(totalElements) /
                                   (SpecialFunctions.Factorial(totalElements - elementsPerCombination) * 
                                    SpecialFunctions.Factorial(elementsPerCombination)));    
        
    }
}