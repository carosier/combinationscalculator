using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace CombinationsCalc.Test
{
    public class ListExtensionsTest
    {
        [Fact]
        public void SelectAllCombinations_WhenSourceDataIsEmpty_ShouldReturnEmptyResult()
        {
            //Act
            var result = new List<int>().SelectAllCombinations(1);
            
            //Assert
            result.Should().BeEmpty();
        }
        
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(3)]
        public void SelectAllCombinations_WhenNumberOfElementsLessOrEqualToZeroOrGreaterThanSourceSize_ShouldReturnEmptyResult(int numberOfElements)
        {
            //Act
            var result = new[]{1,2}.SelectAllCombinations(numberOfElements);
            
            //Assert
            result.Should().BeEmpty();
        }
        
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void SelectAllCombinations_WhenNumberOfElementsIsGreaterThanZeroAndEqualsThanSourceSize_ShouldHaveOneItemWithSameCollection(int numberOfElements)
        {
            //Arrange
            var elements = Enumerable.Range(1,numberOfElements).ToList();
            
            //Act
            var result = elements.SelectAllCombinations(numberOfElements).ToList();
            
            //Assert
            result.Should().HaveCount(1);
            result.First().Should().BeEquivalentTo(elements);
        }

        [Theory]
        [InlineData("1,2,3",1,"1|2|3")]
        [InlineData("1,2,3",2,"1,2|1,3|2,3")]
        [InlineData("1,2,3,4",2,"1,2|1,3|1,4|2,3|2,4|3,4")]
        [InlineData("1,2,3,4",3,"1,2,3|1,2,4|1,3,4|2,3,4")]
        [InlineData("1,2,3,4,5,6,7,8,9",6,
            "1,2,3,4,5,6|1,2,3,4,5,7|1,2,3,4,5,8|1,2,3,4,5,9|1,2,3,4,6,7|1,2,3,4,6,8|1,2,3,4,6,9|1,2,3,4,7,8|1,2,3,4,7,9|1,2,3,4,8,9|1,2,3,5,6,7|1,2,3,5,6,8|1,2,3,5,6,9|1,2,3,5,7,8|1,2,3,5,7,9|1,2,3,5,8,9|1,2,3,6,7,8|1,2,3,6,7,9|1,2,3,6,8,9|1,2,3,7,8,9|1,2,4,5,6,7|1,2,4,5,6,8|1,2,4,5,6,9|1,2,4,5,7,8|1,2,4,5,7,9|1,2,4,5,8,9|1,2,4,6,7,8|1,2,4,6,7,9|1,2,4,6,8,9|1,2,4,7,8,9|1,2,5,6,7,8|1,2,5,6,7,9|1,2,5,6,8,9|1,2,5,7,8,9|1,2,6,7,8,9|1,3,4,5,6,7|1,3,4,5,6,8|1,3,4,5,6,9|1,3,4,5,7,8|1,3,4,5,7,9|1,3,4,5,8,9|1,3,4,6,7,8|1,3,4,6,7,9|1,3,4,6,8,9|1,3,4,7,8,9|1,3,5,6,7,8|1,3,5,6,7,9|1,3,5,6,8,9|1,3,5,7,8,9|1,3,6,7,8,9|1,4,5,6,7,8|1,4,5,6,7,9|1,4,5,6,8,9|1,4,5,7,8,9|1,4,6,7,8,9|1,5,6,7,8,9|2,3,4,5,6,7|2,3,4,5,6,8|2,3,4,5,6,9|2,3,4,5,7,8|2,3,4,5,7,9|2,3,4,5,8,9|2,3,4,6,7,8|2,3,4,6,7,9|2,3,4,6,8,9|2,3,4,7,8,9|2,3,5,6,7,8|2,3,5,6,7,9|2,3,5,6,8,9|2,3,5,7,8,9|2,3,6,7,8,9|2,4,5,6,7,8|2,4,5,6,7,9|2,4,5,6,8,9|2,4,5,7,8,9|2,4,6,7,8,9|2,5,6,7,8,9|3,4,5,6,7,8|3,4,5,6,7,9|3,4,5,6,8,9|3,4,5,7,8,9|3,4,6,7,8,9|3,5,6,7,8,9|4,5,6,7,8,9")]
        public void
            SelectAllCombinations_WhenNumberOfElementsIsGreaterLessThanSourceSize_ShouldReturnAllPossibleCombinations(
                string source, int numberOfElements, string expectedCombinations)
        {
            //Arrange
            var sourceAsList = source.Split(",");

            //Act
            var result = sourceAsList.SelectAllCombinations(numberOfElements).ToList();
            
            //Assert
            var expectedCombinationsList = expectedCombinations.Split("|").ToList().Select(comb => comb.Split(',')).ToList();
            result.Should().HaveSameCount(expectedCombinationsList);
            result.Should().HaveCount(MathUtils.NumberOfCombination(sourceAsList.Length,numberOfElements));
            result.ForEach(combination =>
            {
                expectedCombinationsList.Where(expComb => expComb.Count() == combination.Count())
                    .Any(expComb => expComb.All(element => combination.Contains(element)))
                    .Should().BeTrue();
            });
        }

    }
}