using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace CombinationsCalc
{
    public static class ListExtensions
    {

        public static IEnumerable<IEnumerable<T>> SelectAllCombinations<T>(this IEnumerable<T> source,
            int elementsPerCombination)
        {
            var workArray = source.ToImmutableArray();
            if (elementsPerCombination <= 0 || workArray.Length < elementsPerCombination) return new List<IEnumerable<T>>();
            return new CombinationsCalc<T>(workArray,elementsPerCombination).Combine();   
        }
        
        private class CombinationsCalc<T>
        {
            private readonly ICollection<T> _sourceData;
            private readonly List<int> _currentCombinationIndexes;
            private readonly int _elementsPerCombination;
            private int _lastChangedPosition;
            private int _nextPositionToChange;

            public CombinationsCalc(ICollection<T> sourceData, int elementsPerCombination)
            {
                _sourceData = sourceData;
                _elementsPerCombination = elementsPerCombination;
                _currentCombinationIndexes = new List<int>();
            }

            public IEnumerable<IEnumerable<T>> Combine()
            {
                SetInitialCombination();
                yield return SelectCombinedItems();
                _nextPositionToChange = NextCombinationPositionToChange();
                while (AnyPendingCombination())
                {
                    NextCombination();
                    yield return SelectCombinedItems();
                    _nextPositionToChange = NextCombinationPositionToChange();
                }
            }

            private bool AnyPendingCombination() => _nextPositionToChange > -1;
            

            private IEnumerable<T> SelectCombinedItems() =>
                _currentCombinationIndexes.Select(index => _sourceData.ElementAt(index));


            private void SetInitialCombination()
            {
                _currentCombinationIndexes.AddRange(Enumerable.Range(0, _elementsPerCombination));
                _lastChangedPosition = _elementsPerCombination - 1;
            }


            private void NextCombination()
            {
                _currentCombinationIndexes[_nextPositionToChange] = _currentCombinationIndexes[_nextPositionToChange] + 1;
                if (_lastChangedPosition > _nextPositionToChange)
                {
                    for (var i = _lastChangedPosition; i < _elementsPerCombination ; i++)
                    {
                        _currentCombinationIndexes[i] =
                            _currentCombinationIndexes[_nextPositionToChange] + i - _nextPositionToChange;
                    }
                    
                }
                _lastChangedPosition = _nextPositionToChange;
            }

            private int NextCombinationPositionToChange()
            {
                var offset = 1;
                while (offset <= _elementsPerCombination)
                {
                    if (_currentCombinationIndexes[_elementsPerCombination - offset] !=
                        _sourceData.Count - offset)
                    {
                        return _elementsPerCombination - offset;
                    }

                    offset++;
                }

                return -1;
            }
        }
    }
}